const path = require('path');
const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
require('babel-polyfill');


const production = process.env.NODE_ENV === 'production';
const development = process.env.NODE_ENV === 'development';

const entry = production ?
   ['./src/main.js'] :
[
  'react-hot-loader/patch',
  './src/main.js',
  'webpack/hot/dev-server',
  'webpack-dev-server/client?http://localhost:9898',
];

const plugins = [
  new ProgressBarPlugin(),
];

if (production) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.LoaderOptionsPlugin({ minimize: true })
   );
} else if (development) {
  plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = {
  entry,
  plugins,
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/',
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        exclude: '/node_modules/',
        use: ['url-loader?limit=10000&name=images/[hash:10].[ext]'],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: '/node_modules/',
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        exclude: '/node_modules/',
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        exclude: '/node_modules/',
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?.*$|$)/,
        use: 'file-loader?name=font/[name].[ext]',
      },
    ],
  },
  devtool: development === true ? 'eval' : 'nosources-source-map',
};
