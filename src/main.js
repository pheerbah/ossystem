import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, hashHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import configureStore from './store/storeConfig';
import rootReducer from './reducers/rootReducer';
import App from './components/App';
import GoodsListComp from './components/GoodsListComp';
import createItemComp from './components/CreateItemComp';
import Logining from './components/HelloPage';
import DeniedAccess from './components/DeniedAccess';


import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

injectTapEventPlugin();

const store = configureStore(rootReducer);

render(
  <MuiThemeProvider>
    <Provider store={store}>
      <Router history={hashHistory}>
        <Route path="/" component={Logining} />
        <Route component={App}>
          <Route path="goodslist" component={GoodsListComp} />
          <Route path="createitem" component={createItemComp} />
          <Route path="deniedaccess" component={DeniedAccess} />
        </Route>
      </Router>
    </Provider>
  </MuiThemeProvider>,
document.querySelector('#root'));


