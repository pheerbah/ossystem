import { hashHistory } from 'react-router';
import * as types from '../constants/constants';


import items from '../goods.json';


export const getData = () => (dispatch) => {
  const dataFromStorage = JSON.parse(window.localStorage.getItem('goods'));
  if (dataFromStorage !== null) {
    const itemsInArr = dataFromStorage.items.slice();
    itemsInArr.forEach((el) => {
      let push = false;
      items.items.forEach((element) => {
        if (el.id === element.id) {
          push = false;
          return false;
        }
        push = true;
      });

      if (push) {
        items.items.push(el);
      }
    });
    dispatch({ type: types.ALL_ITEMS, data: items });
  } else {
    dispatch({ type: types.ALL_ITEMS, data: items });
  }
};


export const createNewItem = () => (dispatch) => {
  const dataFromStorage = JSON.parse(window.localStorage.getItem('goods'));
  if (dataFromStorage !== null) {
    const itemsInArr = dataFromStorage.items.slice();
    if (items.items.length === 0) {
      items.items.push(...itemsInArr);
    } else {
      itemsInArr.forEach((el) => {
        let push = false;
        items.items.forEach((element) => {
          if (el.id === element.id) {
            push = false;
            return false;
          }
          push = true;
        });
        if (push) {
          items.items.push(el);
        }
      });
    }
    dispatch({ type: types.ALL_ITEMS, data: items });
  } else {
    dispatch({ type: types.ALL_ITEMS, data: items });
  }
};


export const deleteAllData = data => (dispatch) => {
  items.items = data;
  dispatch({ type: types.DELETE_ALL, data });
};


export const deleteItem = data => (dispatch) => {
  items.items = items.items.filter(el => el.id !== data);
  dispatch({ type: types.DELETE_ITEM });
};

export const checkUser = data => (dispatch) => {
  if (data === 'user' || data === 'admin') {
    hashHistory.push('goodslist');
  }
  dispatch({ type: types.SUCCESS, data });
};
