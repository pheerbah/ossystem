import * as types from '../constants/constants';

const initialState = {
  person: '',
};


const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SUCCESS : return Object.assign({}, state, {
      person: action.data,
    });
    default :
      return state;
  }
};

export default loginReducer;
