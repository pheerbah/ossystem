import * as types from '../constants/constants';

const initialState = {
  items: {},
};


const mainPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ALL_ITEMS : return Object.assign({}, state, {
      items: action.data,
    });
    case types.DELETE_ITEM : return Object.assign({}, state);
    case types.DELETE_ALL : return Object.assign({}, state);
    default :
      return state;
  }
};

export default mainPageReducer;
