import { combineReducers } from 'redux';
import mainPageReducer from './mainPageReducer';
import loginReducer from './loginReducer';



const rootReducer = combineReducers({
  loginReducer,
  mainPageReducer,
});

export default rootReducer;
