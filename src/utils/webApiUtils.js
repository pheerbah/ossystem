import axios from 'axios';



const baseApiUrl = 'http://api.blog.dev.singree.com/';


export const getBlogItems = (resolve, reject) => {
  axios.get(`${baseApiUrl}`)
    .then(response => resolve(response))
    .catch(error => reject(error));
};


export const sendComment = (data, resolve, reject) => {
  axios.post(`${baseApiUrl}comment/`, data)
    .then(response => resolve(response))
    .catch(error => reject(error));
};

export const sendPost = (data, resolve, reject) => {
  axios.post(`${baseApiUrl}`, data)
    .then(response => resolve(response))
    .catch(error => reject(error));
};


export const updatePost = (data, resolve, reject) => {
  axios.put(`${baseApiUrl}${data['_id']}`, data.body)
    .then(response => resolve(response))
    .catch(error => reject(error));
};

