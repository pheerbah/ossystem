import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import * as actions from '../actions/actions';

import '../style/main.scss';

class GoodsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      startIndex: 0,
    };
    this.createItem = this.createItem.bind(this);
  }
  createItem() {
    const itemData = {
      id: Date.now(),
      description: this.description.value,
      name: this.name.value,
      price: parseFloat(this.price.value),
    };
    if (itemData.name !== '' && itemData.description !== '' && !isNaN(itemData.price)) {
      if (JSON.parse(window.localStorage.getItem('goods')) === null) {
        window.localStorage.setItem('goods', JSON.stringify({ items: [] }));
      }
      const objWithItems = JSON.parse(window.localStorage.getItem('goods'));
      objWithItems.items.push(itemData);
      window.localStorage.setItem('goods', JSON.stringify(objWithItems));
    } else {
          console.log('complete fields');
    }
    this.props.create();
  }

  render() {
    return (
      <div style={{ marginTop: 20 }}>
        <Row>
          <Col md={10} mdOffset={1}>
            <div className="additem">
              <div className="additemInputs">
                <input
                  ref={(input) => { this.name = input; }}
                  type="text"
                  placeholder="name of item"
                />
                <input
                  ref={(input) => { this.price = input; }}
                  type="email"
                  placeholder="price of item"
                />
              </div>
              <br />
              <textarea
                ref={(input) => { this.description = input; }}
                type="text"
                placeholder="description of item"
              />
              <div className="additemBtn">
                <RaisedButton label="Create Item" style={{ margin: 0 }} onClick={this.createItem} />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

GoodsList.propTypes = {
  create: PropTypes.func,

};

const mapStateToProps = (state) => {
  const { mainPageReducer } = state;
  return { mainPageReducer };
};

const mapDispatchToProps = dispatch => ({
  create: () => {
    dispatch(actions.createNewItem());
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(GoodsList);
