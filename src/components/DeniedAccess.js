import React from 'react';
import { Row, Col } from 'react-bootstrap';


import '../style/main.scss';

const DeniedAccess = () => (
  <div style={{ marginTop: 200 }}>
    <Row>
      <Col xs={6} xsOffset={3}>
        <p className="deniedEl">Access for this user denied</p>
      </Col>
    </Row>
  </div>
);


export default DeniedAccess;
