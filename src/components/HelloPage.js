import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import * as actions from '../actions/actions';

import '../style/main.scss';

class GoodsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
    };
    this.logIn = this.logIn.bind(this);
  }
  logIn() {
    if (this.logName.value === 'admin' || this.logName.value === 'user') {
      this.setState({ show: true });
      this.props.permit(this.logName.value);
    }
    this.setState({ show: false });
    return false;
  }

  render() {
    return (
      <div style={{ marginTop: 200 }}>
        <Row>
          <Col xs={2} xsOffset={5}>
            <p className="clueForEnter" hidden={this.state.show}>write admin or user</p>
            <input className="inputForEnter" ref={(input) => { this.logName = input; }} type="text" placeholder="write admin or user" />
            <div className="btnForEnter">
              <RaisedButton label="Log IN" onClick={this.logIn} />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

GoodsList.propTypes = {
  permit: PropTypes.func,

};

const mapStateToProps = (state) => {
  const { mainPageReducer } = state;
  return { mainPageReducer };
};

const mapDispatchToProps = dispatch => ({
  permit: (arg) => {
    dispatch(actions.checkUser(arg));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(GoodsList);
