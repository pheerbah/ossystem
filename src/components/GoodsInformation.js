import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import * as actions from '../actions/actions';


class GoodsInformation extends Component {
  constructor(props) {
    super(props);
    this.countPrice = this.countPrice.bind(this);
  }
  componentWillMount() {
    this.props.getInitialValues();
  }
  deleteAllData() {
    window.localStorage.setItem('goods', JSON.stringify({ items: [] }));
    const dataForDelete = [];
    this.props.removeAll(dataForDelete);
  }
  countPrice(el) {
    const totalPrice = el.reduce((sum, item) => {
      return sum + item.price;
    }, 0);
    return totalPrice;
  }
  render() {
    const { mainPageReducer, loginReducer } = this.props;
    const values = mainPageReducer.items.items === undefined ? null : mainPageReducer.items.items;
    return (
      <div className="goodsInfo">
        <p>Total quantity = <span>{values === null ? 0 : values.length}</span> items</p>
        <p>Sumary price = <span>{values === null ? 0 : this.countPrice(values)}</span> $</p>
        <p>Average price = <span>{values === null ? 0 : values.length > 0 ? (this.countPrice(values) / values.length).toFixed(2) : 0}</span> $</p>
        {loginReducer.person === 'admin' ? <RaisedButton label="Delete All" onClick={this.deleteAllData.bind(this)} /> : null}
      </div>
    );
  }
}


GoodsInformation.propTypes = {
  mainPageReducer: PropTypes.object,
  getInitialValues: PropTypes.func,
  removeAll: PropTypes.func,
  loginReducer: PropTypes.object,
};

const mapStateToProps = (state) => {
  const { mainPageReducer, loginReducer } = state;
  return { mainPageReducer, loginReducer };
};

const mapDispatchToProps = dispatch => ({
  getInitialValues: () => {
    dispatch(actions.getData());
  },
  removeAll: (arg) => {
    dispatch(actions.deleteAllData(arg));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(GoodsInformation);
