import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import Pagination from 'react-js-pagination';
import * as actions from '../actions/actions';

import '../style/main.scss';

class GoodsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      startIndex: 0,
    };
    this.renderingItems = this.renderingItems.bind(this);
    this.deleteElement = this.deleteElement.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }
  componentWillMount() {
    this.props.getItems();
  }
  deleteElement(e) {
    if (JSON.parse(window.localStorage.getItem('goods')) !== null) {
      const objWithItems = JSON.parse(window.localStorage.getItem('goods'));
      objWithItems.items = objWithItems.items.filter(el => el.id !== e);
      window.localStorage.setItem('goods', JSON.stringify(objWithItems));
    }
    this.props.removeElement(e);
  }
// logic in pagination
  handlePageChange(pageNumber) {
    if (pageNumber > 1) {
      this.setState({ startIndex: (pageNumber - 1) * 3 });
    } else {
      this.setState({ startIndex: 0 });
    }
    this.setState({ activePage: pageNumber });
  }
  renderingItems(elements, number) {
    const { loginReducer } = this.props;
    if (Array.isArray(elements.items)) {
      const arr = elements.items.map((el, index) => {
        if (index >= number && index <= number + 2) {
          return (
            <Row className="mainSectionRow" key={index} style={{ padding: 0, marginTop: 30 }}>
              <Col xs={11} xsOffset={1} md={5} mdOffset={0} lg={6} lgOffset={0} style={{ padding: 0 }}>
                <div className="leftSideItem">
                  <img width="460" height="260" src={el.url} alt="some text" />
                </div>
              </Col>
              <Col xs={11} xsOffset={1} md={5} mdOffset={2} lg={6} lgOffset={0} style={{ padding: 0 }}>
                <div className="rightSideItem">
                  <span className="title">{el.name}</span>
                  <p className="elBody" >{el.description}</p>
                  {loginReducer.person === 'admin' ? <p onClick={() => { this.deleteElement(el.id); }} className="delete">X</p> : null}
                </div>
              </Col>
            </Row>
          );
        }
      });
      return arr;
    }
  }

  render() {
    const { mainPageReducer } = this.props;
    return (
      <div style={{ marginTop: 20 }}>
        {this.renderingItems(mainPageReducer.items, this.state.startIndex)}
        <Row>
          <Col md={4} mdOffset={4}>
            <div style={{ marginTop: 60 }}>
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={3}
                totalItemsCount={mainPageReducer.items.items !== undefined ? mainPageReducer.items.items.length : 0}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

GoodsList.propTypes = {
  getItems: PropTypes.func,
  mainPageReducer: PropTypes.object,
  removeElement: PropTypes.func,
  loginReducer: PropTypes.object,
};

const mapStateToProps = (state) => {
  const { mainPageReducer, loginReducer } = state;
  return { mainPageReducer, loginReducer };
};

const mapDispatchToProps = dispatch => ({
  getItems: () => {
    dispatch(actions.getData());
  },
  removeElement: (arg) => {
    dispatch(actions.deleteItem(arg));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(GoodsList);
