import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import logo from '../img/logo.png';
import Header from './Header';
import GoodsInformation from './GoodsInformation';


import '../style/main.scss';


const App = props => (
  <div>
    <header>
      <Grid>
        <Row className="show-grid">
          <Col sm={1} md={2}>
            <div className="wraperLogo">
              <img className="logo" src={logo} alt="logo" />
            </div>
          </Col>
          <Col sm={11} md={10}>
            <div className="navigation">
              <ul>
                <li><a href="">ABOUT US</a></li>
                <li><a href="">SERVICES</a></li>
                <li><a href="">GUARANTEE</a></li>
                <li><a href="">RESOURCES</a></li>
                <li><a href="">FORMS</a></li>
                <li><a href="">CONTACT</a></li>
              </ul>
            </div>
          </Col>
        </Row>
      </Grid>
      <Grid bsClass="container-fluid">
        <Row>
          <Col md={12}>
            <Header user={props.loginReducer.person} />
          </Col>
        </Row>
      </Grid>
    </header>
    <main className="mainDoc">
      <Grid>
        <Row>
          <Col md={10}>
            <section>
              {
                (props.children)
              }
            </section>

          </Col>
          <Col md={2} mdPush={1}>
            <GoodsInformation />
          </Col>
        </Row>
      </Grid>
    </main>
    <footer>
      <Grid bsClass="container-fluid">
        <Row>
          <Col xs={7} xsOffset={1} sm={4} smOffset={1} md={3} mdOffset={1}><span>© Windsor Publishing Inc. 2016</span></Col>
          <Col xs={3} xsOffset={1} sm={2} smOffset={5} md={2} mdOffset={6}><span>Contact us</span></Col>
        </Row>
      </Grid>
    </footer>
  </div>
    );

App.propTypes = {
  children: PropTypes.node,
  loginReducer: PropTypes.object,

};

const mapStateToProps = (state) => {
  const { loginReducer } = state;
  return { loginReducer };
};

const mapDispatchToProps = dispatch => ({

});


export default connect(mapStateToProps, mapDispatchToProps)(App);
