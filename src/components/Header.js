import React from 'react';
import AppBar from 'material-ui/AppBar';
import PropTypes from 'prop-types';
import { hashHistory } from 'react-router';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';


const Logged = props => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
  >
    <MenuItem primaryText="Sign out" onClick={() => { hashHistory.push('/'); }} />
  </IconMenu>
);




const NavigationList = props => (
  <IconMenu
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
  >
    <MenuItem primaryText="List of goods" onClick={() => { hashHistory.push('goodslist'); }} />
    <MenuItem primaryText="Add new item" onClick={() => { props.user === 'admin' ? hashHistory.push('createitem') : hashHistory.push('deniedaccess'); }} />
  </IconMenu>
);
NavigationList.propTypes = {
  user: PropTypes.string,
};

Logged.muiName = 'IconMenu';
NavigationList.muiName = 'IconMenu';




const Header = props => (
  <AppBar
    title="SHOP"
    iconElementLeft={<NavigationList user={props.user} />}
    iconElementRight={<Logged />}
  />
    );
Header.propTypes = {
  user: PropTypes.string,
};

export default Header;
